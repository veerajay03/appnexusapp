from src.api import API as API

# from api import API as API
class CallingAppNexus:

	def static_init() -> None:
		return API.static_init()

	def get_AppNexus_data() -> str:
		return API.get_AppNexus_data()


CallingAppNexus.static_init()
CallingAppNexus.get_AppNexus_data()