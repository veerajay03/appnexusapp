from src.util import Util
from src.config import Config
import json, io


class API:


	def static_init() -> None:

		return Config.static_init()


	def get_AppNexus_data() -> None:


		url: str = 'https://api-test.appnexus.com'
		url += '?date=' + Util.today_str() + '&hd=false'
		url += '&api_key=' + Config.get_key()



		data = Util.get_json(url)

		json_data: str = json.JSONEncoder().encode(data)

		print(json_data)


